<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    const DEFAULT_CATEGORIES = [
        'Bug',
        'Crash',
        'Corrupted data',
    ];

    public function run()
    {
        foreach (self::DEFAULT_CATEGORIES as $category) {
            Category::create([
                'name' => $category,
            ]);
        }
    }
}
