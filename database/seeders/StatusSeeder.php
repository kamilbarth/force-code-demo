<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    const DEFAULT_STATUSES = [
        'New',
        'In Progress',
        'Done',
    ];

    public function run()
    {
        foreach (self::DEFAULT_STATUSES as $status) {
            Status::create([
                'name' => $status,
            ]);
        }
    }
}
