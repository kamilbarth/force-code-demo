<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::factory(5)->create();
        User::factory(2)->admin()->create();
        User::factory(2)->employee()->create();

        User::factory()->admin()->create([
            'name' => 'Master Yoda',
            'email' => 'yoda@jedi.com',
        ]);
    }
}
