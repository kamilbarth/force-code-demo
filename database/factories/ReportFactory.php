<?php

namespace Database\Factories;

use App\Enums\RoleEnum;
use App\Models\Category;
use App\Models\Report;
use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReportFactory extends Factory
{
    protected $model = Report::class;

    public function definition(): array
    {
        $categories = Category::all()->pluck('id');
        $statuses = Status::all()->pluck('id');
        $users = User::query()->pluck('id');
        $admins = User::query()->where('role', RoleEnum::ADMIN->name)->pluck('id');

        return [
            'title' => $this->faker->sentence,
            'description' => $this->faker->text,
            'category_id' => $this->faker->randomElement($categories),
            'status_id' => $this->faker->randomElement($statuses),
            'creator_id' => $this->faker->randomElement($users),
            'assignee_id' => $this->faker->randomElement($admins),
        ];
    }

    public function withCategory(Category $category): static
    {
        return $this->state(fn (array $attributes) => [
            'category_id' => $category->getKey(),
        ]);
    }

    public function withStatus(Status $status): static
    {
        return $this->state(fn (array $attributes) => [
            'status_id' => $status->getKey(),
        ]);
    }

    public function withCreator(User $user): static
    {
        return $this->state(fn (array $attributes) => [
            'creator_id' => $user->getKey(),
        ]);
    }

    public function withAssignee(User $user): static
    {
        return $this->state(fn (array $attributes) => [
            'assignee_id' => $user->getKey(),
        ]);
    }
}
