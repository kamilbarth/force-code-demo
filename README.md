<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

# The Force Code demo application

## How to run
```bash
./sail up -d
```
```bash
./sail artisan migrate:fresh --seed
```
```bash
./sail artisan serve
```

> Note: This application is intended to be seen only by The Force Code employees. If you are not a Force Code employee, please do not use this application.
