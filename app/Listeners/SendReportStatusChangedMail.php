<?php

namespace App\Listeners;

use App\Mail\ReportStatusChanged;
use Illuminate\Support\Facades\Mail;

readonly class SendReportStatusChangedMail
{
    public function __construct() {
    }

    public function handle($event): void
    {
        $report = $event->report;
        $oldStatus = $event->oldStatus;
        $newStatus = $event->newStatus;
        $creator = $report->creator;
        $assignee = $report->assignee;

        Mail::to($creator->email)->send(new ReportStatusChanged($report, $creator, $oldStatus, $newStatus));
        Mail::to($assignee->email)->send(new ReportStatusChanged($report, $assignee, $oldStatus, $newStatus));
//        Mail::to($creator->email)->queue(new ReportStatusChanged($report));
    }
}
