<?php

namespace App\Observers;

use App\Events\ReportStatusChanged;
use App\Models\Report;
use App\Repositories\Contracts\StatusRepositoryInterface;

readonly class ReportObserver
{
    public function __construct(
        private StatusRepositoryInterface $statusRepository,
    ) {
    }

    public function updating(Report $report): void
    {
        if (!$report->isDirty('status_id')) {
            return;
        }

        $oldStatus = $this->statusRepository->findOrFail(
            $report->getOriginal('status_id')
        );
        $newStatus = $report->status;

        event(
            new ReportStatusChanged(
                $report,
                $oldStatus,
                $newStatus
            )
        );
    }
}
