<?php

namespace App\Http\Requests\Auth;

use App\Pipelines\Pipes\AuthenticateUser;
use Spatie\LaravelData\Attributes\Validation\Email;
use Spatie\LaravelData\Attributes\Validation\Min;
use Spatie\LaravelData\Attributes\Validation\Required;
use Spatie\LaravelData\Attributes\Validation\StringType;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\DataPipeline;

class LoginRequestData extends Data
{
    public function __construct(
        #[
            Required,
            Email,
            StringType
        ]
        public readonly string $email,
        #[
            Required,
            StringType,
            Min(8),
        ]
        public readonly string $password,
    ) {
    }

    public static function pipeline(): DataPipeline
    {
        return parent::pipeline()
            ->through(AuthenticateUser::class);
    }
}
