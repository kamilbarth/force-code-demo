<?php

namespace App\Http\Requests\Auth;

use App\Models\User;
use App\Pipelines\Pipes\RegisterUser;
use Spatie\LaravelData\Attributes\Validation\Confirmed;
use Spatie\LaravelData\Attributes\Validation\Max;
use Spatie\LaravelData\Attributes\Validation\Min;
use Spatie\LaravelData\Attributes\Validation\Required;
use Spatie\LaravelData\Attributes\Validation\StringType;
use Spatie\LaravelData\Attributes\Validation\Unique;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\DataPipeline;

class RegisterRequestData extends Data
{
    public function __construct(
        #[
            Required,
            StringType,
            Min(3),
            Max(255)
        ]
        public readonly string $name,
        #[
            Required,
            StringType,
            Min(3),
            Max(255),
            Unique(User::class)
        ]
        public readonly string $email,
        #[
            Required,
            StringType,
            Min(8),
            Confirmed
        ]
        public readonly string $password,
    ) {
    }

    public static function pipeline(): DataPipeline
    {
        return parent::pipeline()
            ->through(RegisterUser::class);
    }
}
