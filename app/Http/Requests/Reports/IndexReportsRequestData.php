<?php

namespace App\Http\Requests\Reports;

use Illuminate\Auth\Access\AuthorizationException;
use Spatie\LaravelData\Attributes\WithoutValidation;
use Spatie\LaravelData\Data;

class IndexReportsRequestData extends Data
{
    public function __construct(
        #[WithoutValidation]
        public readonly ?IndexReportsFiltersData $filters = new IndexReportsFiltersData(),
    ) {
    }

    public function statusFilter(): array
    {
        return $this->filters->status;
    }
}
