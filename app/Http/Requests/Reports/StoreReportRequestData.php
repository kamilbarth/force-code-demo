<?php

namespace App\Http\Requests\Reports;

use App\Repositories\Contracts\CategoryRepositoryInterface;
use Illuminate\Validation\Rule;
use Spatie\LaravelData\Attributes\Validation\Max;
use Spatie\LaravelData\Attributes\Validation\Min;
use Spatie\LaravelData\Attributes\Validation\Required;
use Spatie\LaravelData\Attributes\Validation\StringType;
use Spatie\LaravelData\Data;

class StoreReportRequestData extends Data
{
    public function __construct(
        #[
            Required,
            StringType,
            Min(3),
            Max(255)
        ]
        public readonly string $title,
        #[
            Required,
            StringType,
            Min(3),
            Max(255)
        ]
        public readonly string $description,
        #[
            Required,
            StringType,
            Min(3),
            Max(255)
        ]
        public readonly string $category, // this should be validated...
    ) {
    }

    public static function rules(CategoryRepositoryInterface $categoryRepository): array
    {
        $categories = $categoryRepository->index()->pluck('name')->toArray();

        return [
            'category' => [
                Rule::in($categories)
            ],
        ];
    }

    public static function messages(CategoryRepositoryInterface $categoryRepository): array
    {
        $categories = $categoryRepository->index()->pluck('name')->toArray();

        return [
            'category.in' => 'The selected category has to be one of: [' . implode(', ', $categories) . ']',
        ];
    }
}
