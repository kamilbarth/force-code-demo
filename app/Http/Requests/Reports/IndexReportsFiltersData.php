<?php

namespace App\Http\Requests\Reports;

use App\Repositories\Contracts\StatusRepositoryInterface;
use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class IndexReportsFiltersData extends Data
{
    const DEFAULT_STATUS_FILTERS = ['New', 'In progress'];

    public function __construct(
        public ?array $status = null,
    ) {
        if (!$this->status || auth()->user()->cannot('filterReports')) {
            $this->status = self::DEFAULT_STATUS_FILTERS;
        }
    }

    public static function rules(StatusRepositoryInterface $statusRepository): array
    {
        $statuses = $statusRepository->index()->pluck('name')->toArray();

        return [
            'status' => ['array'],
            'status.*' => [
                'string',
                Rule::in($statuses),
            ]
        ];
    }
}
