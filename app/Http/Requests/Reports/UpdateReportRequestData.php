<?php

namespace App\Http\Requests\Reports;

use App\Models\Report;
use App\Models\User;
use App\Pipelines\Pipes\InjectAdminByEmail;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\StatusRepositoryInterface;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Validation\Rule;
use Spatie\LaravelData\Attributes\Validation\Email;
use Spatie\LaravelData\Attributes\Validation\Max;
use Spatie\LaravelData\Attributes\Validation\Min;
use Spatie\LaravelData\Attributes\Validation\Nullable;
use Spatie\LaravelData\Attributes\WithoutValidation;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\DataPipeline;

class UpdateReportRequestData extends Data
{
    #[WithoutValidation]
    public ?User $admin = null;

    public function __construct(
        #[
            Min(3),
            Max(255),
            Nullable,
        ]
        public readonly ?string $title,
        #[
            Min(3),
            Max(255),
            Nullable,
        ]
        public readonly ?string $description,
        #[
            Nullable,
        ]
        public readonly ?string $status,
        #[
            Nullable,
        ]
        public readonly ?string $category,
        #[
            Nullable,
            Email,
        ]
        public readonly ?string $assignee,
    ) {
    }

    public static function pipeline(): DataPipeline
    {
        return parent::pipeline()
            ->through(InjectAdminByEmail::class);
    }

    public static function rules(
        CategoryRepositoryInterface $categoryRepository,
        StatusRepositoryInterface $statusRepository,
    ): array {
        $categories = $categoryRepository->index()->pluck('name')->toArray();
        $statuses = $statusRepository->index()->pluck('name')->toArray();

        return [
            'category' => [
                Rule::in($categories)
            ],
            'status' => [
                Rule::in($statuses)
            ],
        ];
    }

    public static function messages(
        CategoryRepositoryInterface $categoryRepository,
        StatusRepositoryInterface $statusRepository,
    ): array {
        $categories = $categoryRepository->index()->pluck('name')->toArray();
        $statuses = $statusRepository->index()->pluck('name')->toArray();

        return [
            'category.in' => 'The selected category has to be one of: [' . implode(', ', $categories) . ']',
            'status.in' => 'The selected status has to be one of: [' . implode(', ', $statuses) . ']',
        ];
    }
}
