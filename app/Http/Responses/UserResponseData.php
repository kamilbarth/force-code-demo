<?php

namespace App\Http\Responses;

use Spatie\LaravelData\Data;

class UserResponseData extends Data
{
    public function __construct(
        public readonly string $id,
        public readonly string $name,
        public readonly string $email,
        public readonly string $role,
    ) {
    }
}
