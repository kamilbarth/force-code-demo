<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    public function handle(Request $request, Closure $next): JsonResponse
    {
        if (Auth::user() &&  Auth::user()->isAdmin()) {
            return $next($request);
        }

        throw new AuthorizationException('Only administrator can do this.');
    }
}
