<?php

namespace App\Http\Controllers;

use App\Http\Requests\Reports\IndexReportsRequestData;
use App\Http\Requests\Reports\StoreReportRequestData;
use App\Http\Requests\Reports\UpdateReportRequestData;
use App\Http\Responses\ReportResponseData;
use App\Models\Report;
use App\Repositories\Contracts\ReportsRepositoryInterface;

class ReportsController extends Controller
{
    public function __construct(
        private readonly ReportsRepositoryInterface $reportsRepository,
    ) {
    }

    public function index(IndexReportsRequestData $request)
    {
        return ReportResponseData::collection(
            $this->reportsRepository->index($request)
        );
    }

    public function show(Report $report)
    {
        return ReportResponseData::from($report);
    }

    public function store(StoreReportRequestData $request)
    {
        return ReportResponseData::from(
            $this->reportsRepository->store($request)
        );
    }

    public function update(Report $report, UpdateReportRequestData $request)
    {
        return ReportResponseData::from(
            $this->reportsRepository->update($report, $request)
        );
    }

    public function destroy(Report $report)
    {
        return $this->reportsRepository->destroy($report);
    }
}
