<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequestData;
use App\Http\Requests\Auth\RegisterRequestData;
use App\Http\Responses\UserResponseData;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(LoginRequestData $request)
    {
        request()->session()->regenerate();

        return UserResponseData::from(request()->user());
    }

    public function logout(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return response()->json([
            'message' => 'Logged out successfully',
        ]);
    }

    public function register(RegisterRequestData $request): JsonResponse
    {
        return response()->json([
            'user' => request()->user(),
        ]);
    }

    public function user(Request $request)
    {
        return UserResponseData::from(request()->user());
    }
}
