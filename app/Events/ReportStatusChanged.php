<?php

namespace App\Events;

use App\Models\Report;
use App\Models\Status;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ReportStatusChanged
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        public readonly Report $report,
        public readonly Status $oldStatus,
        public readonly Status $newStatus,
    ) {
        //
    }
}
