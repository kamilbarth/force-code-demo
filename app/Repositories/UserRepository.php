<?php

namespace App\Repositories;

use App\Enums\RoleEnum;
use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;

readonly class UserRepository implements UserRepositoryInterface
{

    public function getAdminWithLeastAssignedReports(): ?User
    {
        return User::query()
            ->where('role', '=', RoleEnum::ADMIN)
            ->withCount('assignedReports')
            ->orderBy('assigned_reports_count')
            ->first();
    }

    public function getAdminByEmailOrFail(?string $email): User
    {
        return User::query()
            ->where('email', '=', $email)
            ->where('role', '=', RoleEnum::ADMIN)
            ->firstOrFail();
    }
}
