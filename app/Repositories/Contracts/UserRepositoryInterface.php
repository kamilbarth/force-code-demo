<?php

namespace App\Repositories\Contracts;

use App\Models\User;

interface UserRepositoryInterface
{
    public function getAdminWithLeastAssignedReports(): ?User;

    public function getAdminByEmailOrFail(?string $email): User;
}
