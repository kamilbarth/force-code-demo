<?php

namespace App\Repositories\Contracts;

use App\Http\Requests\Reports\IndexReportsRequestData;
use App\Http\Requests\Reports\StoreReportRequestData;
use App\Http\Requests\Reports\UpdateReportRequestData;
use App\Models\Report;
use Illuminate\Pagination\LengthAwarePaginator;

interface ReportsRepositoryInterface
{
    public function index(IndexReportsRequestData $data): LengthAwarePaginator;

    public function store(StoreReportRequestData $data): Report;

    public function update(Report $report, UpdateReportRequestData $data): Report;

    public function destroy(Report $report): bool;
}
