<?php

namespace App\Repositories\Contracts;

use App\Models\Status;
use Illuminate\Support\Collection;

interface StatusRepositoryInterface
{
    public function findOrFail(string $id): ?Status;

    public function index(): Collection;

    public function getByName(string $name): Status;
}
