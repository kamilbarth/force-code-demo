<?php

namespace App\Repositories;

use App\Models\Status;
use App\Repositories\Contracts\StatusRepositoryInterface;
use Illuminate\Support\Collection;

readonly class StatusRepository implements StatusRepositoryInterface
{
    public function findOrFail(string $id): ?Status
    {
        return Status::query()->findOrFail($id);
    }

    public function index(): Collection
    {
        return Status::all();
    }

    public function getByName(string $name): Status
    {
        return Status::query()->where('name', '=', $name)->firstOrFail();
    }
}
