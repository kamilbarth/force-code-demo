<?php

namespace App\Repositories;

use App\Models\Category;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

readonly class CategoryRepository implements CategoryRepositoryInterface
{
    public function index(): Collection
    {
        return Category::query()->get();
    }

    public function getByName(string $name): Category
    {
        return Category::query()->where('name', '=', $name)->firstOrFail();
    }
}
