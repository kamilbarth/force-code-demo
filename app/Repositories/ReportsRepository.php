<?php

namespace App\Repositories;

use App\Http\Requests\Reports\IndexReportsRequestData;
use App\Http\Requests\Reports\StoreReportRequestData;
use App\Http\Requests\Reports\UpdateReportRequestData;
use App\Models\Report;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\ReportsRepositoryInterface;
use App\Repositories\Contracts\StatusRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

readonly class ReportsRepository implements ReportsRepositoryInterface
{
    public function __construct(
        private CategoryRepositoryInterface $categoryRepository,
        private StatusRepositoryInterface   $statusRepository,
        private UserRepositoryInterface     $userRepository,
    ) {
    }

    public function index(IndexReportsRequestData $data): LengthAwarePaginator
    {
        $user = auth()->user();

        return Report::query()
            ->when($data->statusFilter(), function (Builder $query) use ($data) {
                return $query->whereHas('status', fn ($query) => $query->whereIn('name', $data->statusFilter()));
            })
            ->when(!$user->isAdmin(), fn ($query) => $query->whereBelongsTo($user, 'creator'))
            ->paginate(10);
    }

    public function store(StoreReportRequestData $data): Report
    {
        $report = new Report();
        $report->title = $data->title;
        $report->description = $data->description;
        $report->category()->associate($this->categoryRepository->getByName($data->category));
        $report->status()->associate($this->statusRepository->getByName('New'));
        $report->creator()->associate(auth()->user());
        $report->assignee()->associate($this->userRepository->getAdminWithLeastAssignedReports());

        $report->save();

        return $report;
    }

    public function update(Report $report, UpdateReportRequestData $data): Report
    {
        if ($data->title) {
            $report->title = $data->title;
        }

        if ($data->description) {
            $report->description = $data->description;
        }

        if ($data->status) {
            $report->status()->associate($this->statusRepository->getByName($data->status));
        }

        if ($data->category) {
            $report->category()->associate($this->categoryRepository->getByName($data->category));
        }

        if ($data->admin) {
            $report->assignee()->associate($data->admin);
        }

        $report->save();

        return $report;
    }

    public function destroy(Report $report): bool
    {
        return $report->delete();
    }
}
