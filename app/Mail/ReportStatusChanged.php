<?php

namespace App\Mail;

use App\Models\Report;
use App\Models\Status;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class ReportStatusChanged extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public readonly Report $report,
        public readonly User $user,
        public readonly Status $oldStatus,
        public readonly Status $newStatus,
    ) {
    }

    public function envelope(): Envelope
    {
        return new Envelope(
            from: new Address('yoda@jedi.com', 'Master Yoda'),
            subject: 'Report Status Changed',
        );
    }

    public function content(): Content
    {
        return new Content(
            view: 'mails.reports.status_changed',
            with: [
                'report' => $this->report,
                'user' => $this->user,
                'oldStatus' => $this->oldStatus,
                'newStatus' => $this->newStatus,
            ],
        );
    }

    public function attachments(): array
    {
        return [];
    }
}
