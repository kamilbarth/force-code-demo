<?php

namespace App\Enums;

enum RoleEnum: string
{
    case USER = 'user';
    case EMPLOYEE = 'employee';
    case ADMIN = 'admin';
}
