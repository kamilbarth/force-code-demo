<?php

namespace App\Pipelines\Pipes;

use Illuminate\Auth\Events\Lockout;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Spatie\LaravelData\DataPipes\DataPipe;
use Spatie\LaravelData\Support\DataClass;

class AuthenticateUser implements DataPipe
{
    private string $email;
    private string $password;
    private bool $remember;
    private string $ip;

    public function handle(mixed $payload, DataClass $class, Collection $properties): Collection
    {
        $this->email = $properties->get('email');
        $this->password = $properties->get('password');
        $this->remember = $properties->get('remember', false);
        $this->ip = $properties->get('ip', request()->ip());

        $this->authenticate();
        return $properties;
    }

    private function authenticate(): void
    {
        $this->ensureIsNotRateLimited();

        if (!Auth::attempt(['email' => $this->email, 'password' => $this->password], $this->remember)) {
            RateLimiter::hit($this->throttleKey());

            throw ValidationException::withMessages([
                'email' => trans('auth.failed'),
            ]);
        }

        RateLimiter::clear($this->throttleKey());
    }

    private function ensureIsNotRateLimited(): void
    {
        if (!RateLimiter::tooManyAttempts($this->throttleKey(), 5)) {
            return;
        }

        event(new Lockout(request()));

        $seconds = RateLimiter::availableIn($this->throttleKey());

        throw ValidationException::withMessages([
            'email' => trans('auth.throttle', [
                'seconds' => $seconds,
                'minutes' => ceil($seconds / 60),
            ]),
        ]);
    }

    private function throttleKey(): string
    {
        return Str::transliterate(Str::lower($this->email) . '|' . $this->ip);
    }
}
