<?php

namespace App\Pipelines\Pipes;

use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Spatie\LaravelData\DataPipes\DataPipe;
use Spatie\LaravelData\Support\DataClass;

readonly class InjectAdminByEmail implements DataPipe
{
    public function __construct(
        private UserRepositoryInterface $userRepository,
    ) {
    }

    public function handle(mixed $payload, DataClass $class, Collection $properties): Collection
    {
        $email = Arr::get($payload, 'assignee');
        $admin = $this->userRepository->getAdminByEmailOrFail($email);

        return $properties->put('admin', $admin);
    }
}
