<div>
    Hello, {{ $user->name }}!

    Report "{{ $report->title }}" status has been changed from {{ $oldStatus->name }} to {{ $newStatus->name }}.
</div>
