<?php

use App\Http\Controllers\ReportsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

include __DIR__ . '/auth.php';

Route::prefix('reports')->middleware('auth:sanctum')->group(function() {

    Route::middleware('auth.admin')->group(function() {
        Route::put('/{report}', [ReportsController::class, 'update']);
        Route::delete('/{report}', [ReportsController::class, 'destroy']);
    });

    Route::post('/list', [ReportsController::class, 'index']);
    Route::post('/', [ReportsController::class, 'store']);
    Route::get('/{report}', [ReportsController::class, 'show']);
});
